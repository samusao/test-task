# postgresql helm

#### install

```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install postgresql bitnami/postgresql --namespace devops-assignment -f ./values.yml 
```

#### upgrade

```
helm upgrade postgresql bitnami/postgresql --namespace devops-assignment -f ./values.yml
```

#### cred auth admin user for postgres

```
login: postgres
password: test-postgresql-password
```