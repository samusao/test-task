# test task

required:

1. [helm](https://helm.sh/)
2. [kubectl](https://kubernetes.io/docs/tasks/tools/)
3. [kubernetes](https://kubernetes.io/docs/setup/)

Steps for install manifest application and additional in Kubernetes

1. Create namespace
```
kubectl apply -f namespace.yaml
```

2. Install nginx-ingress-controller

```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx ingress-nginx/ingress-nginx --namespace devops-assignment -f ./helm-charts/nginx-ingress-controller/values.yml
```

3. Install postgres

```
helm install postgresql bitnami/postgresql --namespace devops-assignment -f ./helm-charts/postgres/values.yml
```

4. Install redis

```
helm install redis bitnami/redis --namespace devops-assignment -f ./helm-charts/redis/values.yml
```

5. Install HTTP Server

```
kubectl apply -f ./nginx-service
```

6. Check succuses status
```
http://internal.example.com/status
```