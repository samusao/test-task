# redis helm

#### install

```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install redis bitnami/redis --namespace devops-assignment -f ./values.yml 
```

#### upgrade

```
helm upgrade redis bitnami/redis --namespace devops-assignment -f ./values.yml
```