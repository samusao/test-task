# nginx-ingress-controller helm

#### install

```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx ingress-nginx/ingress-nginx --namespace devops-assignment -f ./values.yml
```

#### upgrade

```
helm upgrade ingress-nginx ingress-nginx/ingress-nginx --namespace devops-assignment -f ./values.yml
```